# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=headscale
pkgver=0.16.0
pkgrel=0
pkgdesc="An open source, self-hosted implementation of the Tailscale control server"
url="https://github.com/juanfont/headscale"
arch="all !ppc64le !riscv64" # go build fails
license="BSD-3-Clause"
pkgusers="headscale"
pkggroups="headscale"
makedepends="go"
subpackages="$pkgname-openrc"
install="$pkgname.pre-install"
source="https://github.com/juanfont/headscale/archive/v$pkgver/headscale-$pkgver.tar.gz
	headscale.initd
	fix-32bit.patch
	"

export GOCACHE="$srcdir/go-cache"
export GOTMPDIR="$srcdir"
export GOMODCACHE="$srcdir/go"

prepare() {
	default_prepare

	# move socket to a subdirectory to allow running as non-root
	sed -i 's|/var/run/headscale.sock|/var/run/headscale/headscale.sock|' config-example.yaml
}

build() {
	make build
}

check() {
	make test
}

package() {
	install -Dm755 headscale "$pkgdir"/usr/bin/headscale

	install -Dm755 "$srcdir"/headscale.initd "$pkgdir"/etc/init.d/headscale
	install -Dm644 config-example.yaml "$pkgdir"/etc/headscale/config.yaml
}

sha512sums="
e0a6908e3edc2717f6c4ff1f60b4c78f213ec316abd33789f78abc84eae1b094c1bdb0a603bb998cb012d0bf1bab99eb207dd3ff8b6a4ea309f19b8bf9c62551  headscale-0.16.0.tar.gz
0800829bfc087af283afc117406324a0129b30b587c8cc5df85e147ac09fc879d726fc2d0b62ed545fb0190ed887641f07256745da9dea56932dd2d90aa41625  headscale.initd
7786cb0110a23a65287587faf736f6242ced8b9527db3358871153626af4fbcd878cdf9a160287685638b1d5c8fce599f199c4c27c2b8e83090bcece6afced02  fix-32bit.patch
"
