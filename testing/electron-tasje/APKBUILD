# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=electron-tasje
pkgver=0.4.0
pkgrel=0
pkgdesc="Tiny replacement for electron-builder"
url="https://codeberg.org/selfisekai/electron_tasje/"
arch="aarch64 x86_64"	# only useful on platforms with electron
license="Apache-2.0"
makedepends="cargo"
options="!check"	# no unit tests yet
source="electron_tasje-$pkgver.tar.gz::https://codeberg.org/selfisekai/electron_tasje/archive/v$pkgver.tar.gz"
builddir="$srcdir/electron_tasje"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

package() {
	install -Dm755 target/release/tasje "$pkgdir"/usr/bin/tasje
}

sha512sums="
1fcfd982e3dae6a5dc2fc3250f97d52f1968b0ed07b4e84d2e85bec2c3694178998f265bcd1ec6c8bbaf245aea0f998ab3ad454e8f1d49b69fc1125dc017de00  electron_tasje-0.4.0.tar.gz
"
