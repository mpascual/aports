# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=sdl2_image
pkgver=2.6.1
pkgrel=0
pkgdesc="A simple library to load images of various formats as SDL surfaces"
url="https://github.com/libsdl-org/SDL_image"
arch="all"
license="Zlib"
makedepends="sdl2-dev libpng-dev libjpeg-turbo-dev
	libwebp-dev tiff-dev zlib-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/libsdl-org/SDL_image/archive/refs/tags/release-$pkgver.tar.gz"
builddir="$srcdir/SDL_image-release-$pkgver"

# secfixes:
#   2.0.5-r1:
#     - CVE-2019-13616
#   2.0.3-r0:
#     - CVE-2017-12122 TALOS-2017-0488
#     - CVE-2017-14440 TALOS-2017-0489
#     - CVE-2017-14441 TALOS-2017-0490
#     - CVE-2017-14442 TALOS-2017-0491
#     - CVE-2017-14448 TALOS-2017-0497
#     - CVE-2017-14449 TALOS-2017-0498
#     - CVE-2017-14450 TALOS-2017-0499

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static \
		--enable-png \
		--enable-png-shared \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
4ce9f28d84a409e13ea8ca1f4a853417cf15efb10d5ca6067cff7d2201f0c4d36040a53adb472d21869653af5b3c08e0df14963299228fdde433245542925e63  sdl2_image-2.6.1.tar.gz
"
